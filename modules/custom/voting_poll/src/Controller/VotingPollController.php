<?php

namespace Drupal\voting_poll\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\voting_poll\VotingPollInterface;

/**
 * Returns responses for voting_poll module routes.
 */
class VotingPollController extends ControllerBase {

  /**
   * Route title callback.
   *
   * @param \Drupal\voting_poll\VotingPollInterface $poll
   *   The poll entity.
   *
   * @return string
   *   The poll label.
   */
  public function pollTitle(VotingPollInterface $poll) {
    return $poll->label();
  }

}
