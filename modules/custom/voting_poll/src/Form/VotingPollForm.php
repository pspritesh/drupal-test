<?php

namespace Drupal\voting_poll\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the voting poll edit forms.
 */
class VotingPollForm extends ContentEntityForm
{
	/**
	 * {@inheritdoc}
	 */
	public function form(array $form, FormStateInterface $form_state)
	{
		$form = parent::form($form, $form_state);
		$voting_poll = $this->entity;

		$form['#title'] = $this->t('Edit @label', ['@label' => $voting_poll->label()]);
		
		foreach ($form['choice']['widget'] as $key => $choice) {
			if (is_int($key) && $form['choice']['widget'][$key]['choice']['#default_value'] != NULL) {
				$form['choice']['widget'][$key]['choice']['#attributes'] = ['class' => ['poll-existing-choice']];
			}
		}

		$form['#attached'] = ['library' => ['voting_poll/admin']];

		return $form;
	}

	/**
	 * {@inheritdoc}
	 */
	public function validateForm(array &$form, FormStateInterface $form_state)
	{
		$voting_poll = $this->buildEntity($form, $form_state);
		$poll_storage = $this->entityManager->getStorage('voting_poll');
		$result = $poll_storage->getPollDuplicates($voting_poll);
		foreach ($result as $item) {
			if (strcasecmp($item->label(), $voting_poll->label()) == 0) {
				$form_state->setErrorByName('question', $this->t('A feed named %feed already exists. Enter a unique question.', array('%feed' => $voting_poll->label())));
			}
		}
		parent::validateForm($form, $form_state);
	}

	/**
	 * {@inheritdoc}
	 */
	public function save(array $form, FormStateInterface $form_state) {
		$voting_poll = $this->entity;
		$insert = (bool) $voting_poll->id();
		$voting_poll->save();
		if ($insert) {
			drupal_set_message($this->t('The poll %voting_poll has been updated.', array('%voting_poll' => $voting_poll->label())));
		}
		else {
			\Drupal::logger('voting_poll')->notice('Poll %voting_poll added.', array('%voting_poll' => $voting_poll->label(), 'link' => $voting_poll->link($voting_poll->label())));
			drupal_set_message($this->t('The poll %voting_poll has been added.', array('%voting_poll' => $voting_poll->label())));
		}

		$form_state->setRedirect('voting_poll.poll_list');
	}
}