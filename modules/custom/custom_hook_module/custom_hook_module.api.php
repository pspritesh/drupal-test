<?php

/**
 * @file
 * Hooks provided by the Custom Hook Module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Act on user account cancellations.
 *
 * This hook is invoked from user_cancel() before a user account is canceled.
 * Depending on the account cancellation method, the module should either do
 * nothing, unpublish content, or anonymize content. See user_cancel_methods()
 * for the list of default account cancellation methods provided by User module.
 * Modules may add further methods via hook_user_cancel_methods_alter().
 *
 * This hook is NOT invoked for the 'user_cancel_delete' account cancellation
 * method. To react to that method, implement hook_ENTITY_TYPE_predelete() or
 * hook_ENTITY_TYPE_delete() for user entities instead.
 *
 * Expensive operations should be added to the global account cancellation batch
 * by using batch_set().
 *
 * @param array $edit
 *   The array of form values submitted by the user.
 * @param \Drupal\Core\Session\AccountInterface $account
 *   The user object on which the operation is being performed.
 * @param string $method
 *   The account cancellation method.
 *
 * @see user_cancel_methods()
 * @see hook_user_cancel_methods_alter()
 */
function hook_hook_module_hello($edit, $account, $method) {
	switch ($method) {
		case 'user_cancel_block_unpublish':
      // Unpublish nodes (current revisions).
		module_load_include('inc', 'node', 'node.admin');
		$nodes = \Drupal::entityQuery('node')
		->condition('uid', $account->id())
		->execute();
		node_mass_update($nodes, ['status' => 0], NULL, TRUE);
		break;

		case 'user_cancel_reassign':
      // Anonymize nodes (current revisions).
		module_load_include('inc', 'node', 'node.admin');
		$nodes = \Drupal::entityQuery('node')
		->condition('uid', $account->id())
		->execute();
		node_mass_update($nodes, ['uid' => 0], NULL, TRUE);
      // Anonymize old revisions.
		db_update('node_field_revision')
		->fields(['uid' => 0])
		->condition('uid', $account->id())
		->execute();
		break;
	}
}

/**
 * @} End of "addtogroup hooks".
 */
