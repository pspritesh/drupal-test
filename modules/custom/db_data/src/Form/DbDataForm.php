<?php

/**
 * Generates a Drupal\db_data\Form\DbDataForm.
 */

namespace Drupal\db_data\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * 
 */
class DbDataForm extends FormBase
{
	
	/**
	 * {inheritdoc}
	 */
	public function getFormId()
	{
		return 'example_form';
	}

	/**
	 * {inheritdoc}
	 */
	public function buildForm(array $form, FormStateInterface $form_state)
	{
		$form['name'] = [
			'#type' => 'textfield',
			'#title' => 'Enter name',
			'#required' => TRUE,
		];
		$form['address'] = [
			'#type' => 'textarea',
			'#title' => 'Enter address',
		];
		$form['contact'] = [
			'#type' => 'tel',
			'#title' => 'Enter contact',
		];
		$form['dob'] = [
			'#type' => 'date',
			'#title' => 'Enter DOB',
			'#required' => TRUE,
		];
		$form['actions']['#type'] = 'actions';
		$form['actions']['submit'] = [
			'#type' => 'submit',
			'#value' => $this->t('Submit'),
			'#button_type' => 'primary',
		];
		return $form;
	}

	/**
	 * {inheritdoc}
	 */
	public function validateForm(array &$form, FormStateInterface $form_state)
	{
		if (strlen($form_state->getValue('contact')) < 8) {
			$form_state->setErrorByName('contact', $this->t('Contact must have atleast 8 digits!'));
		}
	}

	/**
	 * {inheritdoc}
	 */
	public function submitForm(array &$form, FormStateInterface $form_state)
	{
		drupal_set_message($this->t('The data of @user has been stored.', ['@user' => $form_state->getValue('name')]));
	}
}