<?php

/**
 * Generates Drupal\db_data\Controller\DbDataController.
 */

namespace Drupal\db_data\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * 
 */
class DbDataController extends ControllerBase
{
	/**
	 * Generates an example page.
	 */
	public function test()
	{
		return array(
			'#type' => 'markup',
			'#markup' => $this->t( 'Hello World!'),
		);
	}
}