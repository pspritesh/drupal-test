<?php

/* themes/contrib/bootstrap/templates/input/input--form-control.html.twig */
class __TwigTemplate_e62ac555a4de9a54da9fe51f77605f4a81ad30a160ee0479a75243e96ae85cda extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("input.html.twig", "themes/contrib/bootstrap/templates/input/input--form-control.html.twig", 1);
        $this->blocks = array(
            'input' => array($this, 'block_input'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "input.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_19db0dc205a061dc9dd9407e3b6346636dd1356d9cba64cdebc397613a87e758 = $this->env->getExtension("Drupal\\webprofiler\\Twig\\Extension\\ProfilerExtension");
        $__internal_19db0dc205a061dc9dd9407e3b6346636dd1356d9cba64cdebc397613a87e758->enter($__internal_19db0dc205a061dc9dd9407e3b6346636dd1356d9cba64cdebc397613a87e758_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "themes/contrib/bootstrap/templates/input/input--form-control.html.twig"));

        $tags = array("spaceless" => 23, "set" => 25);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('spaceless', 'set'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 23
        ob_start();
        // line 25
        $context["classes"] = array(0 => "form-control");
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_19db0dc205a061dc9dd9407e3b6346636dd1356d9cba64cdebc397613a87e758->leave($__internal_19db0dc205a061dc9dd9407e3b6346636dd1356d9cba64cdebc397613a87e758_prof);

    }

    // line 29
    public function block_input($context, array $blocks = array())
    {
        $__internal_ff6bc4339b23317353bc0acc978ced79d9a436757ed652b9b9170dffae3ca1a9 = $this->env->getExtension("Drupal\\webprofiler\\Twig\\Extension\\ProfilerExtension");
        $__internal_ff6bc4339b23317353bc0acc978ced79d9a436757ed652b9b9170dffae3ca1a9->enter($__internal_ff6bc4339b23317353bc0acc978ced79d9a436757ed652b9b9170dffae3ca1a9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "input"));

        // line 30
        echo "    <input";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["attributes"] ?? null), "addClass", array(0 => ($context["classes"] ?? null)), "method"), "html", null, true));
        echo " />
  ";
        
        $__internal_ff6bc4339b23317353bc0acc978ced79d9a436757ed652b9b9170dffae3ca1a9->leave($__internal_ff6bc4339b23317353bc0acc978ced79d9a436757ed652b9b9170dffae3ca1a9_prof);

    }

    public function getTemplateName()
    {
        return "themes/contrib/bootstrap/templates/input/input--form-control.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 30,  64 => 29,  57 => 1,  54 => 25,  52 => 23,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/contrib/bootstrap/templates/input/input--form-control.html.twig", "/var/www/drupaltest.local.com/Drupal-8.5.1/themes/contrib/bootstrap/templates/input/input--form-control.html.twig");
    }
}
