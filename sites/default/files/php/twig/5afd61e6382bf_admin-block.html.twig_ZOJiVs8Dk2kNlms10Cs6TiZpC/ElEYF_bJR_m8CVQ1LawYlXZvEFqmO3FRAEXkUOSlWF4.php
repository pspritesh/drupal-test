<?php

/* core/themes/stable/templates/admin/admin-block.html.twig */
class __TwigTemplate_a437d6ecc137ffd2ba3709d123667d228d895929eaa0a3fdb12760720cf4b6f2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f7bab6e1846b18711e276a2bd8cf1a7af5f669a39a1daa156bff5ab7dc6cf9b8 = $this->env->getExtension("Drupal\\webprofiler\\Twig\\Extension\\ProfilerExtension");
        $__internal_f7bab6e1846b18711e276a2bd8cf1a7af5f669a39a1daa156bff5ab7dc6cf9b8->enter($__internal_f7bab6e1846b18711e276a2bd8cf1a7af5f669a39a1daa156bff5ab7dc6cf9b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "core/themes/stable/templates/admin/admin-block.html.twig"));

        $tags = array("set" => 17, "if" => 22);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('set', 'if'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 17
        $context["classes"] = array(0 => "panel");
        // line 21
        echo "<div";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["attributes"] ?? null), "addClass", array(0 => ($context["classes"] ?? null)), "method"), "html", null, true));
        echo ">
  ";
        // line 22
        if ($this->getAttribute(($context["block"] ?? null), "title", array())) {
            // line 23
            echo "    <h3 class=\"panel__title\">";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["block"] ?? null), "title", array()), "html", null, true));
            echo "</h3>
  ";
        }
        // line 25
        echo "  ";
        if ($this->getAttribute(($context["block"] ?? null), "content", array())) {
            // line 26
            echo "    <div class=\"panel__content\">";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["block"] ?? null), "content", array()), "html", null, true));
            echo "</div>
  ";
        } elseif ($this->getAttribute(        // line 27
($context["block"] ?? null), "description", array())) {
            // line 28
            echo "    <div class=\"panel__description\">";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["block"] ?? null), "description", array()), "html", null, true));
            echo "</div>
  ";
        }
        // line 30
        echo "</div>
";
        
        $__internal_f7bab6e1846b18711e276a2bd8cf1a7af5f669a39a1daa156bff5ab7dc6cf9b8->leave($__internal_f7bab6e1846b18711e276a2bd8cf1a7af5f669a39a1daa156bff5ab7dc6cf9b8_prof);

    }

    public function getTemplateName()
    {
        return "core/themes/stable/templates/admin/admin-block.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 30,  71 => 28,  69 => 27,  64 => 26,  61 => 25,  55 => 23,  53 => 22,  48 => 21,  46 => 17,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "core/themes/stable/templates/admin/admin-block.html.twig", "/var/www/drupaltest.local.com/Drupal-8.5.1/core/themes/stable/templates/admin/admin-block.html.twig");
    }
}
